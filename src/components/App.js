import React, { Component } from 'react';
import Header         from './Header';
import ListaNoticias  from './ListaNoticias';
import FormularioBusqueda from './FormularioBusqueda';

class App extends Component {

  state = {
    noticias: []
  }

  componentDidMount() {
    this.consultarNoticias();
  }

  consultarNoticias = ( categoria='general' ) => {
    let url = 'https://newsapi.org/v2/top-headlines?country=mx&category=' + categoria + '&apiKey=11021d4a483f4661a78247d03b0b04b9';
    fetch( url )
      .then( res => {
        return res.json();
      } )
      .then( noticias => {
        this.setState( {noticias: noticias.articles} );
      } );
  }

  render() {
    return (
      <div className="contenedor-app">
          <Header titulo='Noticias' />
          <div className='container white contenedor-noticias'>
              <FormularioBusqueda
                  consultarNoticias={this.consultarNoticias} />
              <ListaNoticias 
                  noticias={this.state.noticias} />
          </div>
      </div>
    );
  }
}

export default App;
