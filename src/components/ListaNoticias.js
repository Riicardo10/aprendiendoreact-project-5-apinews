import React, { Component } from 'react';
import Noticia              from './Noticia';
import PropTypes            from 'prop-types';
import {TransitionGroup, CSSTransition} from 'react-transition-group';

class ListaNoticias extends Component {
    render() {
        return (
            <div className='row'>
                <TransitionGroup>
                    { this.props.noticias.map( noticia => {
                        return  (
                            <CSSTransition classNames='fade' timeout={1000} key={noticia.url}>
                                <Noticia 
                                    noticia={noticia} />
                            </CSSTransition>
                        );
                    } ) }
                </TransitionGroup>
            </div>
        );
    }
}

ListaNoticias.propTypes = {
    noticias: PropTypes.array.isRequired
}

export default ListaNoticias;